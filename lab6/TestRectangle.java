public class TestRectangle {
    public static void main(String[] args) {
        Point p = new Point(3,7);
        Rectangle r = new Rectangle(5,6,p);
        System.out.println("area = "+ r.area());

        r.topLeft.xCoord=5;

        Rectangle r2 = new Rectangle(7,9,new Point(3,10));
        System.out.println("area = "+ r2.area());

        System.out.println("perimeter = "+r2.perimeter());
        Point[] corners= r.corners();
        for (int i = 0; i <corners.length;i++)
        {
            System.out.println("x = "+ corners[i].xCoord + " y = "+ corners[i].yCoord);
        }
    }
}
