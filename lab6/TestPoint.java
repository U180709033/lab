public class TestPoint {


    public static void main(String[] args) {
        Point p1= new Point(2,2);
        System.out.println("x = "+ p1.xCoord+", y = "+p1.yCoord);
        Point p2= new Point (2,1);
        System.out.println("x = "+ p2.xCoord+", y = "+p2.yCoord);
        Point p3= new Point(2);
        System.out.println("x = "+ p3.xCoord+", y = "+p3.yCoord);
        Point p4= new Point();
        System.out.println("x = "+ p4.xCoord+", y = "+p4.yCoord);
        System.out.println("distance p1 to p2 = "+ p1.distance(p2));


    }
}
