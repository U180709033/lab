public class Point {
    int xCoord;
    int yCoord;
    public Point(int x, int y){
        xCoord=x;
        yCoord=y;

    }
    public Point(){

    }
    public Point(int xy){
        xCoord = xy;
        yCoord= xy;
    }
    public double distance (Point p){
        return Math.sqrt((xCoord-p.xCoord)*(xCoord-p.xCoord)+(yCoord-p.yCoord)*(yCoord-p.yCoord));

    }


}
