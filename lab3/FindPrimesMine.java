import java.util.Scanner;

public class FindPrimesMine {
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);
        System.out.print("Enter a number: ");
        int number = reader.nextInt();
        int count =0;
        String s="";
        for(int i=2;i<=number;i++){
            for(int j=1;j<i;j++){
                if(i%j==0){
                    count++;
                }

            }
            if(count<2)
            {
                if (s=="")
                {
                    s=s+i;
                }
                else{
                    s=s+","+ i;
                }

            }
            count=0;
        }
        System.out.println(s);
    }
}
